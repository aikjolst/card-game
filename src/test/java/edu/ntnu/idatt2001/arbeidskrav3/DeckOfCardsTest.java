package edu.ntnu.idatt2001.arbeidskrav3;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

  @Test
  void deckOfCardsTest_constructor() {
    DeckOfCards cards = new DeckOfCards();
    String cardsAsString = cards.getAsString();
    System.out.println(cardsAsString);

    assertTrue(true);
  }

  @Test
  void dealHand_test_two_different_hands(){
    int numberOfcards = 5;
    DeckOfCards cards = new DeckOfCards();
    HandOfCards handOne = cards.dealHand(numberOfcards);
    HandOfCards handTwo = cards.dealHand(numberOfcards);

    assertNotEquals(handOne,handTwo);
  }

  @Test
  void dealHand_number_of_cards_not_between_1_and_52(){
    int numberOfcards = 55;
    DeckOfCards cards = new DeckOfCards();

    assertThrows(IllegalArgumentException.class, () -> cards.dealHand(numberOfcards));
  }

  @Test
  void deck_of_cards_contains_52_cards(){
    DeckOfCards deck = new DeckOfCards();
    assertEquals(52, deck.getCardCount());
  }

  @Test
  void dealHand_removes_cards_from_deck(){
    DeckOfCards deck = new DeckOfCards();
    deck.dealHand(5);
    assertEquals(47, deck.getCardCount());
  }


}