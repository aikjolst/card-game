package edu.ntnu.idatt2001.arbeidskrav3;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    void getAsString() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H',12));
        cards.add(new PlayingCard('S',2));
        HandOfCards hand = new HandOfCards(cards);

        assertEquals("H12 S2", hand.getAsString());
    }

    @Test
    void check_if_hand_is_flush_true() {
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 12));
        playingCards.add(new PlayingCard('H', 13));
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('H', 2));
        playingCards.add(new PlayingCard('H', 5));
        HandOfCards cards = new HandOfCards(playingCards);

        assertTrue(cards.checkIfFlush());

    }

    @Test
    void check_if_hand_is_flush_false() {
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 12));
        playingCards.add(new PlayingCard('S', 13));
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('H', 2));
        playingCards.add(new PlayingCard('H', 5));
        HandOfCards cards = new HandOfCards(playingCards);

        assertFalse(cards.checkIfFlush());

    }

    @Test
    void queen_of_spade_in_hand_true(){
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 12));
        playingCards.add(new PlayingCard('S', 12));
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('C', 2));
        playingCards.add(new PlayingCard('H', 5));
        HandOfCards cards = new HandOfCards(playingCards);

        assertTrue(cards.isQueenOfSpadesOnHand());
    }

    @Test
    void queen_of_spade_in_hand_false(){
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 12));
        playingCards.add(new PlayingCard('H', 12));
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('C', 2));
        playingCards.add(new PlayingCard('H', 5));
        HandOfCards cards = new HandOfCards(playingCards);

        assertFalse(cards.isQueenOfSpadesOnHand());
    }

    @Test
    void sum_of_the_cards_on_hand(){
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('H', 2));
        playingCards.add(new PlayingCard('H', 3));
        playingCards.add(new PlayingCard('C', 1));
        playingCards.add(new PlayingCard('H', 4));
        HandOfCards cards = new HandOfCards(playingCards);
        int numberSummed = 11;

        assertEquals(numberSummed, cards.getTheSumOfTheCards());
    }

    @Test
    void cards_that_are_hearts(){
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('H', 1));
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('C', 3));
        playingCards.add(new PlayingCard('C', 1));
        playingCards.add(new PlayingCard('D', 4));
        HandOfCards cards = new HandOfCards(playingCards);

        assertEquals("H1 ", cards.getCardThatHaveSuitHearts());
    }

    @Test
    void cards_that_with_out_hearts(){
        List<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('D', 1));
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('C', 3));
        playingCards.add(new PlayingCard('C', 1));
        playingCards.add(new PlayingCard('D', 4));
        HandOfCards cards = new HandOfCards(playingCards);

        assertNotEquals("H1", cards.getCardThatHaveSuitHearts());
    }

}