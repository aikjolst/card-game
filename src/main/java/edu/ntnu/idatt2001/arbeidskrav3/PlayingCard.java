package edu.ntnu.idatt2001.arbeidskrav3;

/**
 * This class represents a playing card
 *
 * @author Adele
 */
public class PlayingCard {
  private final char suit;
  private final int face;

  /**
   * The constructor of a new card.
   *
   * @param suit the typre of the card (Heart, diamonds, clover, spades)
   * @param face the number of the card
   */
  public PlayingCard(char suit, int face){
    this.suit = suit;
    this.face = face;
  }

  /**
   * Method that returns a playing card as string.
   *
   * @return playing card as a string
   *
   */
  public String getAsString() {
    return String.format("%s%s", suit, face);
  }

  /**
   * This method returns the suit of the card.
   *
   * @return the suit of the card
   *
   */
  public char getSuit() {
    return suit;
  }

  /**
   * This method returns the number of the card.
   *
   * @return the number of the card
   *
   */
  public int getFace() {
    return face;
  }
}
