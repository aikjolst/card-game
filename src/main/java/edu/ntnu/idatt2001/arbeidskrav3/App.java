package edu.ntnu.idatt2001.arbeidskrav3;

import javafx.application.Application;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.scene.Scene;

import java.util.ArrayList;


public class App extends Application {
  private GameModel model = new GameModel();
  private HandOfCards hand = new HandOfCards(new ArrayList<>());

  public static void start(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) {
    Region sceneRoot = new ScreenBuilder(model,() -> dealHand(), () -> checkHand()).build();
    Scene scene = new Scene(sceneRoot,500,400);
    stage.setScene(scene);
    scene.getStylesheets().add("/css/default.css");
    stage.show();
  }

  public void dealHand(){
    model.reset();
    DeckOfCards deck = new DeckOfCards();
    deck.shuffle();
    hand = deck.dealHand(5);
    model.setHandText(hand.getAsString());

  }

  public void checkHand(){
    if (hand.checkIfFlush()){
      model.setFlush("Yes");
    } else {
      model.setFlush("No");
    }

    if (hand.isQueenOfSpadesOnHand()){
      model.setQueenOfSpades("Yes");
    } else {
      model.setQueenOfSpades("No");
    }

    model.setSum(String.valueOf(hand.getTheSumOfTheCards()));

    model.setHearts(hand.getCardThatHaveSuitHearts());
  }
}