package edu.ntnu.idatt2001.arbeidskrav3;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a hand of cards.
 *
 * @author Adele
 */
public class HandOfCards {
    List<PlayingCard> cards = new ArrayList<>();

    /**
     * The constructor of a new hand of cards.
     *
     * @param cards List of playing cards
     *
     */
   public HandOfCards(List<PlayingCard> cards){
       this.cards = cards;
   }

    /**
     * Method that returns a list of cards as å string.
     *
     * @return playingcards as a string
     *
     */
    public String getAsString() {
        return String.join(" ",cards.stream().map(PlayingCard::getAsString).toList());
    }

    /**
     * Method that returns the sum of the cards.
     *
     * @return the sum of the cards as an int
     *
     */
    public int getTheSumOfTheCards(){
       int sum = 0;

        for (PlayingCard card : cards) {
            int number = card.getFace();
            sum += number;
        }

        return sum;
    }

    /**
     * Method that returns if the queen of spades are one of the cards on hand.
     *
     * @return true if queen of spades is one of the cards and false if its not
     *
     */
    public boolean isQueenOfSpadesOnHand(){
       boolean queenOfSpades = false;

        for (PlayingCard card : cards) {
            if (card.getFace() == 12 && card.getSuit() == 'S'){
                queenOfSpades = true;
            }
        }

        return queenOfSpades;
    }

    /**
     * Method that returns if you have flush on hand.
     *
     * @return true if you have flush and false if not
     *
     */
    public boolean checkIfFlush(){
       boolean flush = true;
       char firstChar = (cards.get(0)).getSuit();

        for (PlayingCard card : cards) {
            if (firstChar != card.getSuit()){
                flush = false;
            }
        }

       return flush;
    }

    /**
     * Method that returns the cards with heart.
     *
     * @return the cards with heart as a string
     *
     */
    public String getCardThatHaveSuitHearts(){
       ArrayList<PlayingCard> cardsWithHarts = new ArrayList<>();
       char heart = 'H';
       StringBuilder text = new StringBuilder();

        for (PlayingCard card : cards) {
            if (heart == card.getSuit()){
                cardsWithHarts.add(card);
            }
        }
        if (cardsWithHarts.isEmpty()){
            text.append("No hearts");
        } else {
            for (PlayingCard card : cardsWithHarts) {
                text.append(card.getAsString());
                text.append(" ");
            }
        }

        return String.valueOf(text);
    }

}
