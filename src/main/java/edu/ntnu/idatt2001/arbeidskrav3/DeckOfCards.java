package edu.ntnu.idatt2001.arbeidskrav3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * This class represents a deck of cards.
 *
 * @author Adele
 */
public class DeckOfCards {
  private ArrayList<PlayingCard> cards;
  private final char[] suit = {'S','H','D','C'};

  /**
   * This constructor creates a new deck of cards.
   *
   */
  public DeckOfCards() {
    cards = new ArrayList<>(52);
    for (char type : suit) {
      for (int i = 1; i < 14; i++) {
        PlayingCard playingCard = new PlayingCard(type, i);
        cards.add(playingCard);
      }
    }
  }

  /**
   * This method returns the amount of cards.
   *
   * @return the amount of cards as an int
   *
   */
  public int getCardCount(){
    return cards.size();
  }

  /**
   * Method that returns a list of cards as å string.
   *
   * @return Playing cards as a string
   *
   */
  public String getAsString() {
    return String.join(" ",cards.stream().map(PlayingCard::getAsString).toList());
  }

  /**
   * This method shuffle cards.
   *
   */
  public void shuffle(){
    ArrayList<PlayingCard> shuffledCards = new ArrayList<>();

    while (cards.size() > 0){
      Random random = new Random();
      int number = random.nextInt(cards.size());
      shuffledCards.add(cards.get(number));
      cards.remove(number);
    }
    cards = shuffledCards;

  }

  /**
   * This method deal a hand of cards.
   *
   * @param n the amount of cards
   *
   * @return a hand of n cards
   *
   */
  public HandOfCards dealHand(int n){
    if (n < 1 || n > cards.size()){
      throw new IllegalArgumentException("This is not a valid number.");
    }
    List<PlayingCard> hand = new ArrayList<>(n);
    for (int i = 0; i < n; i++) {
      hand.add(cards.remove(i));
    }
    return new HandOfCards(hand);
  }



}
