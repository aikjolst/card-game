package edu.ntnu.idatt2001.arbeidskrav3;

import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Builder;
import javafx.scene.control.Button;

public class ScreenBuilder implements Builder<Region> {
    private GameModel model;
    private Runnable dealHand;
    private Runnable checkHand;

    public ScreenBuilder(GameModel model, Runnable dealHand, Runnable checkHand){
        this.model = model;
        this.checkHand = checkHand;
        this.dealHand = dealHand;

    }

    @Override
    public Region build(){
        BorderPane results = new BorderPane();
        Node headingBox = createTop();
        BorderPane.setMargin(headingBox, new Insets(4));
        results.setTop(createTop());
        results.setCenter(createHandTextField());
        results.setBottom(createBottomOfTheScreen());
//        results.setBottom(createButtonGridPane());
        return results;
    }

    private GridPane createButtonGridPane(){
        GridPane results = new GridPane();
        results.setAlignment(Pos.BOTTOM_RIGHT);
        results.addColumn(2,createButtonCheckHand(),createButtonDealHand());
        return results;
    }

    private GridPane createBottomOfTheScreen(){
        GridPane results = new GridPane();
        results.setAlignment(Pos.BOTTOM_CENTER);
        results.addRow(2,createTextFieldGridPane(),createButtonGridPane());
        results.setHgap(230);
        results.setPadding(new Insets(10));
        return results;
    }

    private GridPane createTextFieldGridPane(){
        GridPane results = new GridPane();
        results.setAlignment(Pos.BOTTOM_LEFT);
        results.add(new Label("Flush:"), 1,0);
        results.add(createTextField(model.flushProperty()), 3,0);

        results.add(new Label("Cards of hearts:"), 1,1);
        results.add(createTextField(model.heartsProperty()), 3, 1);

        results.add(new Label("Sum of the faces:"), 1,2);
        results.add(createTextField(model.sumProperty()), 3,2);

        results.add(new Label("Queen of spades:"), 1,3);
        results.add(createTextField(model.queenOfSpadesProperty()), 3,3);
        return results;
    }

    private Node createTextField(StringProperty property){
        Text text = new Text();
        text.textProperty().bindBidirectional(property);
        return text;
    }

    private Node createHandTextField(){
        Text text = new Text();
        text.textProperty().bindBidirectional(model.handTextProperty());
        text.setStyle("-fx-font-size: 20;");
        text.setTextAlignment(TextAlignment.JUSTIFY);
        return text;
    }

    private Node createTop() {
        return new Text(" ");
    }

    private Node createButtonDealHand(){
        HBox results = new HBox();
        results.setPadding(new Insets(8));
        Button button = new Button("Deal hand");
        button.setOnAction((ActionEvent event)-> this.dealHand.run());

        results.getChildren().add(button);

        return results;
    }

    private Node createButtonCheckHand(){
        HBox results = new HBox();
        results.setPadding(new Insets(8));
        Button button = new Button("Check hand");
        button.setOnAction((ActionEvent event)-> this.checkHand.run());
        results.getChildren().add(button);
        return results;
    }



}

