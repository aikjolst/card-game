package edu.ntnu.idatt2001.arbeidskrav3;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class GameModel {
    private StringProperty handText = new SimpleStringProperty();
    private StringProperty sum = new SimpleStringProperty();
    private StringProperty flush = new SimpleStringProperty();
    private StringProperty queenOfSpades = new SimpleStringProperty();
    private StringProperty hearts = new SimpleStringProperty();

    public void setSum(String sum) {
        this.sum.set(sum);
    }

    public void setFlush(String flush) {
        this.flush.set(flush);
    }

    public void setQueenOfSpades(String queenOfSpades) {
        this.queenOfSpades.set(queenOfSpades);
    }

    public void setHearts(String hearts) {
        this.hearts.set(hearts);
    }

    public StringProperty sumProperty() {
        return sum;
    }

    public StringProperty flushProperty() {
        return flush;
    }

    public StringProperty queenOfSpadesProperty() {
        return queenOfSpades;
    }

    public StringProperty heartsProperty() {
        return hearts;
    }

    public void setHandText(String handText) {
        this.handText.set(handText);
    }

    public StringProperty handTextProperty(){
        return handText;
    }

    public void reset(){
        setHearts("");
        setSum("");
        setFlush("");
        setQueenOfSpades("");
        setHandText("");
    }
}
